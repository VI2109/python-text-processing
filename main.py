# imports needed for Tk
from tkinter import *
from tkinter import messagebox
from tkinter import ttk

# Main class, all logic happens here
class Processor:
    # Constructor, runs at the very beginning
    def __init__(self, root): 
        # Changing window title
        root.title("Обработка текста")
        # Setting up the main frame
        mainframe = ttk.Frame(root, padding='3')
        mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        # Internal variables related to the text
        self._words = []
        self._active_word_index = 0
        self._text = StringVar()
        # Internal variables related to delimiters
        self._delimiter = '~'
        self._with_delimiters = BooleanVar()
        # Label containing the text
        ttk.Label(mainframe, textvariable=self._text, font="TkFixedFont", takefocus=0)\
            .grid(column=0, columnspan=3, row=0, sticky=(W, E))
        # Checkbutton controlling the delimiters
        ttk.Checkbutton(mainframe, text='Разделители', variable=self._with_delimiters, onvalue=True, offvalue=False, command=self.draw)\
            .grid(column=2, row=1, sticky=(W, E))
        # Buttons for text navigation
        ttk.Button(mainframe, text='<-', command=self.move_cursor_left, takefocus=1)\
            .grid(column=0, row=2, sticky=(W, E))
        ttk.Button(mainframe, text='Найти', command=self.find_word)\
            .grid(column=1, row=2, sticky=(W, E))
        ttk.Button(mainframe, text='->', command=self.move_cursor_right)\
            .grid(column=2, row=2, sticky=(W, E))
        # Buttons for text modification
        ttk.Button(mainframe, text='Вставить', command=self.insert_word)\
            .grid(column=0, row=3, sticky=(W, E))
        ttk.Button(mainframe, text='Изменить', command=self.change_word)\
            .grid(column=1, row=3, sticky=(W, E))
        ttk.Button(mainframe, text='Удалить', command=self.delete_word)\
            .grid(column=2, row=3, sticky=(W, E))

            # The above commands create the UI elements and set them up in a grid like this
            # +--------------------------------------------+
            # |                 Main label                 |
            # +--------------+--------------+--------------+
            # |              |              |[-] Delimiters|
            # +--------------+--------------+--------------+
            # |      <-      |    Search    |      ->      |
            # +--------------+--------------+--------------+
            # |    Insert    |    Replace   |    Delete    |
            # +--------------+--------------+--------------+

        # Making padding between UI elements bigger
        for child in mainframe.winfo_children():
            child.grid_configure(padx=5, pady=5)
        # Setting up keyboard bindings
        root.bind('<Left>', self.move_cursor_left)
        root.bind('j', self.move_cursor_left)
        root.bind('<Right>', self.move_cursor_right)
        root.bind('k', self.move_cursor_right)
        root.bind('f', self.find_word)
        root.bind('s', self.change_word)
        root.bind('<Return>', self.insert_word)
        root.bind('a', self.insert_word)
        root.bind('<Delete>', self.delete_word)
        root.bind('d', self.delete_word)
        # Calling draw, it sets the label correctly
        self.draw()

    # Method `draw`, update the label text according to the internal variables
    # Should be called after changes to the internal variables
    def draw(self, *args):
        cursor_draw_index = 0
        text = ''
        delimiter = ' ' if not self._with_delimiters.get() else self._delimiter
        for index, word in enumerate(self._words):
            text += word + delimiter
            if index < self._active_word_index:
                cursor_draw_index += len(word) + 1
        text = text[:-1]
        text += '\n'
        text += ' '*cursor_draw_index + '^'
        self._text.set(text)

    # Method `move_cursor_left`, moves cursor one word to the left
    def move_cursor_left(self, *args):
        self._active_word_index = max(0, self._active_word_index - 1)
        self.draw()

    # Method `move_cursor_right`, moves cursor one word to the right
    def move_cursor_right(self, *args):
        self._active_word_index = min(len(self._words), self._active_word_index + 1)
        self.draw()

    # Method `insert_word`, inserts the word before the active one
    def insert_word(self, *args):
        # Get the word from the user with the word dialog
        word = self.word_dialog('Вставить слово')
        # Insert the word into the array
        i = self._active_word_index
        self._words = self._words[:i] + word.split() + self._words[i:]
        # Move cursor to the right for convenience
        self.move_cursor_right() 
        # draw() called at the end of `move_cursor_right`

    # Method `find_word`, attempts to find the word and make it active
    #                     messagebox is shown on a failed attempt
    def find_word(self, *args):
        # Get the word from the user with the word dialog
        word = self.word_dialog('Найти слово')
        # Trying to find the word
        try:
            self._active_word_index = self._words.index(word)
        except ValueError:
            # If word not found, show an error
            messagebox.showerror(message='Слово не найдено')
        self.draw()

    # Method `change_word`, changed the active word to 
    def change_word(self, *args):
        if self._active_word_index < len(self._words):
            # Get the word from the user with the word dialog
            word = self.word_dialog('Изменить слово', self._words[self._active_word_index])
            # Change the active word to it
            self._words[self._active_word_index] = word
            self.draw()

    # Method `delete_word`, deletes the active word
    def delete_word(self, *args):
        # Checking that we are deleting a word inside the array
        if len(self._words) > 0 and self._active_word_index < len(self._words):
            self._words.pop(self._active_word_index)
            # Move cursor to the left for convenience
            self.move_cursor_left()
            # draw() called at the end of `move_cursor_right`

    # Method `word_dialog`, shows user a dialog with a word entry, returns the word, entered by the user
    # arguments are `title` - the title of the dialog and the text on the button,
    #               `contents` - the contents of the entry, default is empty
    def word_dialog(self, title, contents='', *args) -> str:
        # Creating the window ant setting up the UI
        dialog = Toplevel(root)
        dialog.title(title)

        dialog_frame = ttk.Frame(dialog, padding='3')
        dialog_frame.grid(column=0, row=0, sticky=(N, W, E, S))
        dialog.columnconfigure(0, weight=1)
        dialog.rowconfigure(0, weight=1)
        
        def dismiss(*args):
            dialog.grab_release()
            dialog.destroy()

        word = StringVar(value=contents)
        word_entry = ttk.Entry(dialog_frame, width=18, textvariable=word)
        word_entry.selection_range(0, len(contents))
        word_entry.grid(column=1, row=1, sticky=(W, E))
        ttk.Button(dialog_frame, text=title, command=dismiss)\
            .grid(column=1, row=2, sticky=(W, E))
        word_entry.focus()

        for child in dialog_frame.winfo_children():
            child.grid_configure(padx=5, pady=5)
        
        dialog.bind("<Return>", dismiss)
        # Binds the window closing to the right callback
        dialog.protocol("WM_DELETE_WINDOW", dismiss)
        # Sets stuff up so that the dialog stops the execution inside the main window
        dialog.transient(root)
        dialog.wait_visibility()
        dialog.grab_set()
        dialog.wait_window()
        
        return word.get()


if __name__ == '__main__':
    root = Tk()
    Processor(root)
    root.mainloop()
